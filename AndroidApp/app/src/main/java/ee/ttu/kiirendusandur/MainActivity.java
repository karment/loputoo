package ee.ttu.kiirendusandur;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.shimmerresearch.android.Shimmer;
import com.shimmerresearch.android.guiUtilities.ShimmerBluetoothDialog;
import com.shimmerresearch.android.manager.ShimmerBluetoothManagerAndroid;
import com.shimmerresearch.bluetooth.ShimmerBluetooth;
import com.shimmerresearch.driver.CallbackObject;
import com.shimmerresearch.driver.Configuration;
import com.shimmerresearch.driver.ObjectCluster;
import com.shimmerresearch.driverUtilities.ChannelDetails;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

import static android.widget.Toast.LENGTH_LONG;
import static com.shimmerresearch.android.guiUtilities.ShimmerBluetoothDialog.EXTRA_DEVICE_ADDRESS;


public class MainActivity extends Activity {

    ShimmerBluetoothManagerAndroid btManager;
    private String bluetoothAdd = "";
    private final static String LOG_TAG = "ObjectClusterExample";
    private final static int PERMISSIONS_REQUEST_WRITE_STORAGE = 5;

    //Write to CSV variables
    private FileWriter fw;
    private BufferedWriter bw;
    private File file;
    boolean firstTimeWrite = true;

    //Firebase variables
    private DatabaseReference reference;

    private static DecimalFormat decimalFormat = new DecimalFormat(".#####");
    private double startTime;
    private boolean firstTimeStream = true;
    private boolean setStartTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseApp.initializeApp(getApplicationContext());
        reference = FirebaseDatabase.getInstance().getReference();
        reference.removeValue();

        try {
            btManager = new ShimmerBluetoothManagerAndroid(this, mHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Check if permission to write to external storage has been granted
        if (Build.VERSION.SDK_INT >= 23) {
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_WRITE_STORAGE);
            }
        }

    }

    public void connectDevice(View v) {
        Intent intent = new Intent(getApplicationContext(), ShimmerBluetoothDialog.class);
        startActivityForResult(intent, ShimmerBluetoothDialog.REQUEST_CONNECT_SHIMMER);
    }

    public void startStreaming(View v) {
        //Setup CSV writing
        String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        //String baseDir = android.os.Environment.getDataDirectory().getAbsolutePath();
        String fileName = "ObjectClusterExample Data " + DateFormat.getDateTimeInstance().format(new Date()) + ".csv";
        String filePath = baseDir + File.separator + fileName;
        file = new File(filePath);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            fw = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fw);
        } catch(IOException e) {
            e.printStackTrace();
        }

        Shimmer shimmer = (Shimmer) btManager.getShimmer(bluetoothAdd);
        //Disable PC timestamps for better performance. Disabling this takes the timestamps on every full packet received instead of on every byte received.
        shimmer.enablePCTimeStamps(false);
        //Enable the arrays data structure. Note that enabling this will disable the Multimap/FormatCluster data structure
        shimmer.enableArraysDataStructure(true);
        btManager.startStreaming(bluetoothAdd);
    }

    public void stopStreaming(View v) {
        try {   //Stop CSV writing
            bw.flush();
            bw.close();
            fw.close();
            firstTimeWrite = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        btManager.stopStreaming(bluetoothAdd);
    }

    /**
     * Get the result from the paired devices dialog
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                //Get the Bluetooth mac address of the selected device:
                bluetoothAdd = data.getStringExtra(EXTRA_DEVICE_ADDRESS);
                btManager.connectShimmerThroughBTAddress(bluetoothAdd); //Connect to the selected device

            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case ShimmerBluetooth.MSG_IDENTIFIER_DATA_PACKET:
                    if ((msg.obj instanceof ObjectCluster)) {
                        ObjectCluster objc = (ObjectCluster) msg.obj;

                        /**
                         * ---------- Printing a channel to Logcat ----------
                         */
                        //Method 1 - retrieve data from the ObjectCluster using get method
                        double data = objc.getFormatClusterValue(Configuration.Shimmer3.ObjectClusterSensorName.ACCEL_WR_X, ChannelDetails.CHANNEL_TYPE.CAL.toString());
                        double dataY = objc.getFormatClusterValue(Configuration.Shimmer3.ObjectClusterSensorName.ACCEL_WR_Y, ChannelDetails.CHANNEL_TYPE.CAL.toString());
                        double dataZ = objc.getFormatClusterValue(Configuration.Shimmer3.ObjectClusterSensorName.ACCEL_WR_Z, ChannelDetails.CHANNEL_TYPE.CAL.toString());
                        double dataTime = objc.getFormatClusterValue(Configuration.Shimmer3.ObjectClusterSensorName.TIMESTAMP, ChannelDetails.CHANNEL_TYPE.CAL.toString());

                        String accelX = Configuration.Shimmer3.ObjectClusterSensorName.ACCEL_WR_X;
                        String accelY = Configuration.Shimmer3.ObjectClusterSensorName.ACCEL_WR_Y;
                        String accelZ = Configuration.Shimmer3.ObjectClusterSensorName.ACCEL_WR_Z;
                        String timestamp = Configuration.Shimmer3.ObjectClusterSensorName.TIMESTAMP;

                        if (setStartTime) {
                            startTime = dataTime;
                            setStartTime = false;
                        }

                        double time = ((dataTime - startTime)/1000);

                        Log.i(LOG_TAG, accelX + " data: " + data);
                        Log.i(LOG_TAG, accelY + " data: " + dataY);
                        Log.i(LOG_TAG, accelZ + " data: " + dataZ);
                        Log.i(LOG_TAG, timestamp + " timestamp: " + time);

                        String keyTimestamp = String.valueOf(new Timestamp(System.currentTimeMillis()).getTime());
                        reference.child(keyTimestamp).setValue(decimalFormat.format(data) + ";" + decimalFormat.format(dataY)
                                + ";" + decimalFormat.format(dataZ) + ";" + decimalFormat.format(time));

                        /**
                         * ---------- Writing all channels of CAL data to CSV file ----------
                         */
                        if(firstTimeWrite) {
                            //Write headers on first-time
                            for(String channelName : objc.sensorDataArray.mSensorNames) {
                                try {
                                    bw.write(channelName + ",");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            try {
                                bw.write("\n");
                            } catch(IOException e2) {
                                e2.printStackTrace();
                            }
                            firstTimeWrite = false;
                        }
                        for(double calData : objc.sensorDataArray.mCalData) {
                            String dataString = String.valueOf(calData);
                            try {
                                bw.write(dataString + ",");
                            } catch(IOException e3) {
                                e3.printStackTrace();
                            }
                        }
                        try {
                            bw.write("\n");
                        } catch(IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                    break;
                case ShimmerBluetooth.MSG_IDENTIFIER_STATE_CHANGE:
                    ShimmerBluetooth.BT_STATE state = null;
                    String macAddress = "";
                    Button startStreaming = (Button) findViewById(R.id.button2);
                    Button stopStreaming = (Button) findViewById(R.id.button3);

                    if (msg.obj instanceof ObjectCluster) {
                        state = ((ObjectCluster) msg.obj).mState;
                        macAddress = ((ObjectCluster) msg.obj).getMacAddress();
                    } else if (msg.obj instanceof CallbackObject) {
                        state = ((CallbackObject) msg.obj).mState;
                        macAddress = ((CallbackObject) msg.obj).mBluetoothAddress;
                    }

                    if (state == ShimmerBluetooth.BT_STATE.SDLOGGING && firstTimeStream) {
                        Toast.makeText(getApplicationContext(), "Shimmer is now ready for streaming!", Toast.LENGTH_SHORT).show();
                        startStreaming.setEnabled(true);
                        stopStreaming.setEnabled(true);
                        firstTimeStream = false;
                    }

                    else if (state == ShimmerBluetooth.BT_STATE.DISCONNECTED) {
                        Toast.makeText(getApplicationContext(), "Connection failed, please try again!", Toast.LENGTH_SHORT).show();
                    }

                    else if (state == ShimmerBluetooth.BT_STATE.STREAMING_AND_SDLOGGING) {
                        startStreaming.setPressed(true);
                    }
                    else if (state == ShimmerBluetooth.BT_STATE.SDLOGGING && !firstTimeStream) {
                        startStreaming.setPressed(false);
                    }

                    /*switch (state) {
                        case CONNECTED:
                            break;
                        case CONNECTING:
                            break;
                        case STREAMING:
                            break;
                        case STREAMING_AND_SDLOGGING:
                            break;
                        case SDLOGGING:
                            break;
                        case DISCONNECTED:
                            break;
                    }*/
                    break;
            }
            super.handleMessage(msg);
        }
    };

    /**
     * Permission request callback
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSIONS_REQUEST_WRITE_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Error! Permission not granted. App will now close", LENGTH_LONG).show();
                finish();
            }
        }
    }


}